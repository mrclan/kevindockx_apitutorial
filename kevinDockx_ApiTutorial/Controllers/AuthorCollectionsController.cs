﻿using AutoMapper;
using kevinDockx_ApiTutorial.Models;
using kevinDockx_ApiTutorial.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace kevinDockx_ApiTutorial.Controllers
{
    [Route("api/authorcollections")]
    public class AuthorCollectionsController : Controller
    {
        ILibraryRepository _libraryRepository;

        public AuthorCollectionsController(ILibraryRepository libraryRepository)
        {
            _libraryRepository = libraryRepository;
        }

        public IActionResult CreateAuthorsCollection([FromBody]IEnumerable<AuthorForCreationDto> authors)
        {
            if (authors == null)
            {
                return BadRequest();
            }

            var authorsEntities = Mapper.Map<IEnumerable<Entities.Author>>(authors);
            
            foreach (var author in authorsEntities)
            {
                _libraryRepository.AddAuthor(author);
            }

            if (!_libraryRepository.Save())
            {
                throw new Exception("An error occurred while creating author collection.");
            }

            var authorDtoCollection = Mapper.Map<IEnumerable<AuthorDto>>(authorsEntities);

            return Ok(authorDtoCollection);
        }
    }
}
