﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kevinDockx_ApiTutorial.Entities
{
    public static class LibraryContextExtension
    {
        public static void EnsureSeedDataForContext(this LibraryContext context)
        {
            // first clear the database. This ensures that we always start fresh with each demo.
            // Not advised for production environments, obviously :-)

            context.Authors.RemoveRange(context.Authors);
            context.SaveChanges();

            // init seed data
            var authors = new List<Author>
            {
                new Author()
                {
                    Id = new Guid("9c6e0702-9147-4336-96e6-b6d3c0a87700"),
                    FirstName = "Stephen",
                    LastName = "King",
                    Genre = "Horror",
                    DateOfBirth = new DateTimeOffset(new DateTime(1947,9,21)),
                    Books = new List<Book>
                    {
                        new Book()
                        {
                            Id = new Guid("cee222d0-1ce9-4414-8bcf-5b5939d01c44"),
                            Title = "The Shining",
                            Description = "The shining is a horror novel by American author Stephen King."
                        },
                        new Book()
                        {
                            Id = new Guid("ccfa2919-cad1-4dd8-a0e3-45d2f46afe6e"),
                            Title = "Misery",
                            Description = "Misery is a 1987 psychological horror novel by Stephen King."
                        },
                        new Book()
                        {
                            Id = new Guid("935c6f86-4efb-49fb-9039-b2e761eecefb"),
                            Title = "It",
                            Description = "It is another horror novel published in 1986."
                        },
                        new Book()
                        {
                            Id = new Guid("df0769d1-ece3-4764-a491-3b4405a07727"),
                            Title = "The Stand",
                            Description = "Yet another post-apocalyptic horror/fantasy novel by Stephen King."
                        }
                    }
                },
                new Author()
                {
                    Id = new Guid("42b1e562-82d4-472f-9013-d45fa0d7745a"),
                    FirstName = "George",
                    LastName = "RR Martin",
                    Genre = "Fantasy",
                    DateOfBirth = new DateTimeOffset(new DateTime(1948,9,20)),
                    Books = new List<Book>
                    {
                        new Book()
                        {
                            Id = new Guid("c8aa644d-b58b-4516-afaf-4ce3569e0763"),
                            Title = "Super Mario",
                            Description = "An adventure filled novel based in Italian lifestyle."
                        },
                        new Book()
                        {
                            Id = new Guid("a4c1428c-d8d6-4b5e-a268-df25cf427f65"),
                            Title = "Really Cool",
                            Description = "A comedy novel by George RR Martin."
                        }
                    }
                }
            };

            context.Authors.AddRange(authors);
            context.SaveChanges();
        }
    }
}
