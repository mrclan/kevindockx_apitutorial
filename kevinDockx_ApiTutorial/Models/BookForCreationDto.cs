﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kevinDockx_ApiTutorial.Models
{
    public class BookForCreationDto
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
